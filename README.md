# dotfiles

My personal dotfiles.

  * .vimrc
  * .gitconfig

However, the plan is to get this into our SaltStack configuration management.

# Inspired by
 * https://github.com/michaeljsmalley/dotfiles
 * http://blog.smalleycreative.com/tutorials/using-git-and-github-to-manage-your-dotfiles/
 * https://github.com/sd65/MiniVim


